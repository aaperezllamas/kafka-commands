# Practica Kafka

## Ejercicio 1 - Utilidades de línea de comando

- Lista todos los topics del cluster **con detalle** usando la utilidad `kafka-topics`.

```bash
kafka-topics --zookeeper master01.bigdata.alumnos.upcont.es --describe 
```

- Crea un topic con 1 replica, 3 particiones y retención de 1 mes (30*24*60*60*1000), con nombre `streaming.alumnos.<user>.network`:

```bash
kafka-topics --zookeeper master01.bigdata.alumnos.upcont.es --create --topic streaming.alumnos.aalvarez.network --partitions 3 --replication-factor 1 --config retention.ms=25920000000
```
 
- Crea un topic con 1 replica, 2 particiones y retención de 1 mes, con nombre `streaming.alumnos.<user>.logs`:

```bash
kafka-topics --zookeeper master01.bigdata.alumnos.upcont.es --create --topic streaming.alumnos.aalvarez.logs --partitions 2 --replication-factor 1 --config retention.ms=25920000000
```

Arranca el consumidor en línea de comando y suscribete al topic `streaming.books` con la linea siguiente:

```bash
kafka-console-consumer --bootstrap-server master01.bigdata.alumnos.upcont.es:9092 --topic streaming.books
```

- ¿Por qué no consume ningún mensaje existente en el topic? Añade lo que falte a la línea anterior para ver todo el contenido del topic

```bash
No se consume ningun mensaje debido a que esta puesto por defecto auto.offset.reset = latest, se solucion con --from-beginning    
```

Ahora limita la salida a los 100 primeros mensajes del topic añadiendo la opción `--max-messages 100`.
¿Por qué salen desordenados? Si el orden es necesario, ¿que solución propondrías?

[RESPUESTA]
```bash
kafka-console-consumer --bootstrap-server master01.bigdata.alumnos.upcont.es:9092 --topic streaming.books --max-messages 100 --from-beginning

Si es necesario mantener el ordern habria que enviar-consumir todos los eventos en los que se quiera mantener el orden de la misma partición. Por lo que en este caso para mantener el orden en un libro habria que enviar todos los eventos correspondientes a eventos de un libro por una misma partición.
```


Usando la herramienta `kafka-console-consumer`, consume todos los registros generados sobre el topic `streaming.network.records`, especificando
como `group.id` tu nombre de usuario. Escribe a continuacion la linea necesaria para ejecutar la herramienta:

```bash
kafka-console-consumer --bootstrap-server master01.bigdata.alumnos.upcont.es:9092 --new-consumer --topic streaming.books --consumer-property group.id=aalvarez --from-beginning
```

A continuación utiliza la herramienta `kafka-consumer-groups.sh` (situada en `/opt/cloudera/parcels/KAFKA/lib/kafka/bin`) 
para obtener la información relacionada con el `group.id` especificado y comprueba el offset de cada particion y 
que el lag es 0 para todas las particiones de este grupo.

Copia a continuacion la salida de la herramienta kafka-consumer-groups:

```bash
./kafka-consumer-groups.sh --bootstrap-server master01.bigdata.alumnos.upcont.es:9092 --describe --group aalvarez
```
|TOPIC                   |    PARTITION | CURRENT-OFFSET | LOG-END-OFFSET | LAG    |    CONSUMER-ID  |                  HOST                    |       CLIENT-ID|
|		:-:				    |			:-:	  |      :-:        |     :-:        |  :-:      |   :-:             |                    :-:              |    :-:
|streaming.books         |       7      | 11796      |        11796    |       0   |       -          |                    -                    |               -
|streaming.books         |       10     |   11795    |        11795    |       0   |       -          |                    -                    |          -
|streaming.books         |       17     |    11799   |        11799    |       0   |       -          |                    -                    |          -
|streaming.books         |       4      |    11798   |        11798    |       0   |       -          |                    -                    |          -
|streaming.books         |       0      |    11799   |        11799    |       0   |       -          |                    -                    |          -
|streaming.books         |       8      |    11799   |        11799    |       0   |       -          |                    -                    |          -
|streaming.books         |       11     |    11799   |        11799    |       0   |       -          |                    -                    |          -
|streaming.books         |       13     |    11798   |        11798    |       0   |       -          |                    -                    |          -
|streaming.books         |       5      |    11800   |        11800    |       0   |       -          |                    -                    |          -
|streaming.books         |       15     |    11797   |        11797    |       0   |       -          |                    -                    |          -
|streaming.books         |       18     |    11797   |        11797    |       0   |       -          |                    -                    |          -
|streaming.books         |       2      |    11800   |        11800    |       0   |       -          |                    -                    |          -
|streaming.books         |       9      |    11798   |        11798    |       0   |       -          |                    -                    |          -
|streaming.books         |       12     |    11796   |        11796    |       0   |       -          |                    -                    |          -
|streaming.books         |       6      |    11797   |        11797    |       0   |       -          |                    -                    |          -
|streaming.books         |       14     |    11800   |        11800    |       0   |       -          |                    -                    |          -
|streaming.books         |       1      |    11795   |        11795    |       0   |       -          |                    -                    |          -
|streaming.books         |       16     |    11795   |        11795    |       0   |       -          |                    -                    |          -
|streaming.books         |       3      |    11798   |        11798    |       0   |       -          |                    -                    |          -
|streaming.books                19      |   11796    |       11796     |       0   |       -          |                     -                   |           -

